# Flask Pipenv Container Test
This project is intented to create an example of using flask with pipenv inside Docker container.

### Building image via DockerFile
docker build -t flask-pipenv-test .

### Creating container from built image
docker run --rm -p 5001:5000 --name flask_pipenv_test -d flask-pipenv-test 